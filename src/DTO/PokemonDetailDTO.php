<?php

namespace App\DTO;

use Pokemon\Models\Card;
use Pokemon\Models\CardImages;

class PokemonDetailDTO
{
	private string $id;
	private string $name;
	private ?array $types;
	private ?array $resistances;
	private ?array $weaknesses;
	private ?array $attacks;
	private ?CardImages $images;

	public function __construct(Card $item)
	{
		$this->id = $item->getId();
		$this->name = $item->getName();
		$this->types = $item->getTypes();
		$this->resistances = $item->getResistances();
		$this->weaknesses = $item->getWeaknesses();
		$this->attacks = $item->getAttacks();
		$this->images = $item->getImages();
	}

	public function toArray()
	{
		return [
			'id' => $this->id,
			'name' => $this->name,
			'types' => $this->types,
			'resistances' => $this->resistances,
			'weaknesses' => $this->weaknesses,
			'attacks' => $this->attacks,
			'images' => $this->images->toArray()
		];

	}

	public function setId(string $id)
	{
		$this->id = $id;
	}

	public function getId(): string
	{
		return $this->id;
	}

	public function setName(string $name)
	{
		$this->name = $name;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function setTypes(array $types)
	{
		$thisCardImages->types = $types;
	}

	public function getTypes(): array
	{
		return $this->types;
	}

	public function setResistances(array $resistances)
	{
		$this->resistances = $resistances;
	}

	public function getResistances(): ?array
	{
		return $this->resistances;
	}

	public function setWeaknesses(array $weaknesses)
	{
		$this->weaknesses = $weaknesses;
	}

	public function getWeaknesses(): ?array
	{
		return $this->weaknesses;
	}

	public function setAttacks(array $attacks)
	{
		$this->attacks = $attacks;
	}

	public function getAttacks(): ?array
	{
		return $this->attacks;
	}

	public function setImages(CardImages $images)
	{
		$this->images = $images;
	}

	public function getImages(): CardImages
	{
		return $this->images;
	}
}
