<?php

namespace App\Controller;

use App\Service\PokemonCardService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

use App\Controller\ApiExceptionHandlerTrait;
use App\DTO\PokemonListDTO;
use App\DTO\PokemonDetailDTO;

class PokemonApiController extends AbstractController
{
    use ApiJsonEncoderTrait;
    use ApiExceptionHandlerTrait;

    private $pokemonCardService;

    /**
     * Constructor for PokemonApiController.
     *
     * @param PokemonCardService $pokemonCardService The service for fetching Pokemon card data.
     */
    public function __construct(PokemonCardService $pokemonCardService)
    {
        $this->pokemonCardService = $pokemonCardService;
    }

    /**
     * Handles the listing of Pokemon cards through the API.
     *
     * @param Request $request The request object containing pagination parameters.
     * @return JsonResponse The JSON response containing the list of Pokemon cards.
     */
    #[Route('/api/v1/cards', name: 'api_card_list')]
    public function list(Request $request): JsonResponse
    {
        $page = $request->query->getInt('page', 1);
        $cards = $this->pokemonCardService->getAllCards($page, 300);
        if (!$cards) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }
        $collection = new PokemonListDTO($cards);
        $response = [];
        $jsonEncoder = new JsonEncoder();
        foreach ($collection as $item) {
            try {
                $response[] = $this->encodeJson($item->toArray());
            } catch (\JsonException $e) {
                return $this->handleException($e);
            }
        }
        return new JsonResponse($response, Response::HTTP_OK);
    }

    /**
     * Handles the retrieval of a specific Pokemon card's details through the API.
     *
     * @param string $id The ID of the Pokemon card to retrieve.
     * @return JsonResponse The JSON response containing the details of the Pokemon card.
     */
    #[Route('/api/v1/card/{id}', name: 'api_card_detail')]
    public function detail(string $id): JsonResponse
    {
        $card = $this->pokemonCardService->getCardById($id);
        $item = new PokemonDetailDTO($card);
        if (!$card) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }
        $jsonEncoder = new JsonEncoder();
        try {
            $cleanedJson = $this->encodeJson($item->toArray());
        } catch (\JsonException $e) {
            return $this->handleException($e);
        }
        return new JsonResponse(json_decode($cleanedJson, true), Response::HTTP_OK);
    }
}
