<?php

namespace App\DTO;

class PaginationDTO
{
	private int $page;
	private int $pageSize;
	private int $totalPages;

	public function __construct(int $size, int $total)
	{
		$this->pageSize = $size;
		$this->totalPages = $total;
	}

	public function getPage()
	{
		return $this->page;
	}

	public function setPage(int $page)
	{
		$this->page = $page;
	}

	public function getPageSize()
	{
		return $this->pageSize;
	}

	public function setPageSize(int $size)
	{
		$this->pageSize = $size;
	}

	public function getTotalPages()
	{
		// return $this->totalPages;
		return 250;
	}

	public function setTotalPages(int $total)
	{
		$this->totalPages = $total;
	}
}