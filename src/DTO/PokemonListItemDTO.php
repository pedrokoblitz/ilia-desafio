<?php

namespace App\DTO;

use Pokemon\Models\Card;
use Pokemon\Models\CardImages;

class PokemonListItemDTO
{
	private string $id;
	private string $name;
	private ?array $images;
	private ?array $types;

	public function __construct(Card $item)
	{
		$this->id = $item->getId();
		$this->name = $item->getName();
		$this->types = $item->getTypes();
		$this->images = $item->getImages() ? $item->getImages()->toArray() : null;
	}

	public function toArray()
	{
		return [
			'id' => $this->id,
			'name' => $this->name,
			'images' => $this->images,
			'types' => $this->types
		];
	}

	public function setId(string $id)
	{
		$this->id = $id;
	}

	public function getId(): string
	{
		return $this->id;
	}

	public function setName(string $name)
	{
		$this->name = $name;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function setTypes(array $types)
	{
		$this->types = $types;
	}

	public function getTypes(): array
	{
		return $this->types;
	}

	public function setImages(CardImages $images)
	{
		$this->images = $images->toArray();
	}

	public function getImages(): array
	{
		return $this->images;
	}
}