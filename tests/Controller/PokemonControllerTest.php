<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PokemonControllerTest extends WebTestCase
{
    public function testFirstPokemonCardDetails(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/cards');

        $this->assertResponseIsSuccessful();

        // Assert the card title
        $this->assertSelectorTextContains('.card-title', 'Grookey');

        // Assert the card ID
        $this->assertSelectorTextContains('.card-text.text-muted', 'swshp-SWSH001');

        // Assert the card types
        $this->assertSelectorTextContains('.card-text.mb-0', 'Types: Grass');
    }

    public function testFirstPokemonCardDetailPage(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/card/swshp-SWSH001');

        $this->assertResponseIsSuccessful();

        // Assert the card title
        $this->assertSelectorTextContains('h1', 'Grookey');

        // Assert the card ID
        $this->assertSelectorTextContains('.card-header span', 'swshp-SWSH001');

        // Assert the card title in the card body
        $this->assertSelectorTextContains('.card-body .card-title', 'Grookey');

        // Assert the card types
        $this->assertSelectorTextContains('.card-body .card-text.mb-0', 'Types: Grass');

        // Assert the card resistances
        $this->assertSelectorTextContains('.card-body .card-text.mb-2 span', 'Resistances:');

        // Assert the card attacks
        $this->assertSelectorTextContains('.card-body ul.list-group-flush li', 'Branch Poke');
    }
}
