<?php

namespace App\Tests\Controller;

use App\Controller\PokemonApiController;
use App\Service\PokemonCardService;
use Pokemon\Models\Card;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Component\Cache\Adapter\ArrayAdapter;

class PokemonApiControllerTest extends WebTestCase
{
    public function testList(): void
    {
        $client = static::createClient();
        $cache = new ArrayAdapter();
        $pokemonCardService = new PokemonCardService('test_api_key', 3600, $cache, true);
        $controller = new PokemonApiController($pokemonCardService);

        $request = new Request();
        $request->query->set('page', 1);
        $response = $controller->list($request);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getContent(), true);
        $this->assertIsArray($data);
        $this->assertNotEmpty($data);
    }

    public function testDetail(): void
    {
        $client = static::createClient();
        $cache = new ArrayAdapter();
        $pokemonCardService = new PokemonCardService('test_api_key', 3600, $cache, true);
        $controller = new PokemonApiController($pokemonCardService);

        $card = new Card('Gardevoir', ['Fairy']);
        $response = $controller->detail('xy7-54');

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getContent(), true);
        $this->assertIsArray($data);
        $this->assertEquals('Gardevoir', $data['name']);
        $this->assertEquals(['Fairy'], $data['types']);
    }
}
