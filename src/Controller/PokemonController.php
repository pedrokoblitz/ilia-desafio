<?php

namespace App\Controller;

use App\Service\PokemonCardService;
use App\DTO\PaginationDTO;
use App\DTO\PokemonListDTO;
use App\DTO\PokemonDetailDTO;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PokemonController extends AbstractController
{
    private PokemonCardService $pokemonCardService;
    private PaginationDTO $pagination;
    private int $currentPage;

    /**
     * Constructor for PokemonController.
     *
     * @param PokemonCardService $pokemonCardService The service for fetching Pokemon card data.
     * @param PaginationDTO $pagination The DTO for handling pagination.
     */
    public function __construct(PokemonCardService $pokemonCardService, PaginationDTO $pagination)
    {
        $this->pokemonCardService = $pokemonCardService;
        $this->pagination = $pagination;
    }

    /**
     * Parses the list request to set the current page for pagination.
     *
     * @param Request $request The request object containing page information.
     */
    protected function parseListRequest(Request $request)
    {
        $page = $request->query->getInt('page', 1); 
        $this->pagination->setPage($page);
    }

    /**
     * Prepares template variables for the list view.
     *
     * @param PokemonListDTO $cards The list of Pokemon cards to display.
     * @return array Template variables for rendering the list view.
     */
    protected function getListTplVars(PokemonListDTO $cards): array
    {
        $page = $this->pagination->getPage();
        $totalPages = $this->pagination->getTotalPages();
        
        $tplVars = [
            'cards' => $cards->toArray(),
            'currentPage' => $page,
            'totalPages' => $totalPages,
            'pageRange' => range(max(1, $page - 2), min($totalPages, $page + 2)),             
        ];
        return $tplVars;
    }

    /**
     * Prepares template variables for the item detail view.
     *
     * @param PokemonDetailDTO $card The detailed information of a Pokemon card.
     * @return array Template variables for rendering the item detail view.
     */
    protected function getItemTplVars(PokemonDetailDTO $card): array
    {
        $tplVars = [
            'card' => $card,
        ];
        return $tplVars;
    }

    /**
     * Displays a list of Pokemon cards with pagination.
     *
     * @param Request $request The request object.
     * @return Response The response containing the rendered list view.
     */
   #[Route('/', name: 'home')]
   #[Route('/cards', name: 'card_list')]
   public function list(Request $request): Response
    {
        $this->parseListRequest($request);
        $page = $this->pagination->getPage();
        $pageSize = $this->pagination->getPageSize();
        $totalPages = $this->pagination->getTotalPages();
        $cards = $this->pokemonCardService->getAllCards($page, $pageSize);
        $collection = new PokemonListDTO($cards);
        $data = $this->getListTplVars($collection);
        return $this->render('pokemon/list.html.twig', $data);
    }

    /**
     * Displays the detailed information of a specific Pokemon card.
     *
     * @param string $id The ID of the Pokemon card to display.
     * @return Response The response containing the rendered detail view.
     */
    #[Route('/card/{id}', name: 'card_detail')]
    public function detail(string $id): Response
    {
        $card = $this->pokemonCardService->getCardById($id);
        $item = new PokemonDetailDTO($card);
        $data = $this->getItemTplVars($item);
        return $this->render('pokemon/detail.html.twig', $data);
    }
}
