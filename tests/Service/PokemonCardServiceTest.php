<?php

namespace App\Tests\Service;

use Pokemon\Models\Card;
use App\Service\PokemonCardService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Contracts\Cache\CacheInterface;

class PokemonCardServiceTest extends KernelTestCase
{
    private $cache;

    protected function setUp(): void
    {
        parent::setUp();
        $this->cache = $this->createMock(CacheInterface::class);
    }

    public function testServiceInitialization(): void
    {
        $kernel = self::bootKernel();
        $cardService = static::getContainer()->get(PokemonCardService::class);
        $this->assertInstanceOf(PokemonCardService::class, $cardService);
    }

    public function testGetCardList(): void
    {
        $pokemonCardService = new PokemonCardService('test_api_key', 3600, $this->cache, false);

        $cards = $pokemonCardService->getAllCards(1, 20);
        $this->assertCount(20, $cards);

        // Assert properties of the first card
        $this->assertNotNull($cards[0]->getName());
        $this->assertEquals('Grookey', $cards[0]->getName());
        $this->assertNotNull($cards[0]->getId());
        $this->assertIsArray($cards[0]->getTypes());
        $this->assertNotEmpty($cards[0]->getTypes());

        // Assert properties of the second card
        $this->assertNotNull($cards[1]->getName());
        $this->assertEquals('Scorbunny', $cards[1]->getName());
        $this->assertNotNull($cards[1]->getId());
        $this->assertIsArray($cards[1]->getTypes());
        $this->assertNotEmpty($cards[1]->getTypes());
    }

    public function testGetCardById(): void
    {
        $cardId = 'xy7-54';
        $pokemonCardService = new PokemonCardService('test_api_key', 3600, $this->cache, false);

        $card = $pokemonCardService->getCardById($cardId);

        // Assert properties of the card
        $this->assertNotNull($card->getName());
        $this->assertEquals('Gardevoir', $card->getName());
        $this->assertNotNull($card->getId());
        $this->assertIsArray($card->getTypes());
        $this->assertNotEmpty($card->getTypes());
        $this->assertContains('Fairy', $card->getTypes());
    }
}
