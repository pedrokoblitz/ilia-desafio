# Desafio Ília

## Instalação e execução de testes

- instalar a cli do symfony conforme documentação do framework

```sh
git clone git@gitlab.com:pedrokoblitz/ilia-desafio.git
composer install
symfony server:start
php vendor/bin/phpunit
```

## Estrutura

### Rotas
  home               /                  (essa rota mapeia o msm método da rota /cards)               
  card_list          /cards                         
  card_detail        /card/{id}                
  api_card_list      /api/v1/cards             
  api_card_detail    /api/v1/card/{id}

### Camada de Controlador

- Controlador para as páginas e adicionei templates com Twig com as informações descritas na documentação 
- Controlador para a api foi um plus, apenas repassei os dados como vinham da api externa, utilizando a biblioteca de serialização do framework

### Camada de DTO

- Foram adicionados DTOs para passagem de dados entre serviço e controlador.

### Tratos da API

- Foram criados 2 tratos para funções genéricas da Api: lidar com exceções e serializar Json.

### Camada de Serviço

- Classe simples para abstração da api externa
- Cache para melhorar performance com opção para desabilitar (especialmente para rodar os testes)

### Templates

- Utilizei Twig e Bootstrap, não achei necessário adicionar CSS extra. apenas uma fonte do Google.

### Testes

- Após abstrair mais as funcionalidades de cada classe para funções protegidas/internas das classes, a aplicação se tornou mais testável. Apenas métodos públicos foram testados.

- Foram adicionadas mais asserções para checar a integridade do tráfego de HTTP e o conteúdo completo de pelo menos 1 dos cartões com as informações do item. 

