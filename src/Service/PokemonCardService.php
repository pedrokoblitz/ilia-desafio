<?php

namespace App\Service;

use Pokemon\Pokemon;
use Pokemon\Models\Card;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

/**
 * Service class for handling Pokemon card data retrieval.
 */
class PokemonCardService
{
    private string $apiKey;
    private CacheInterface $cache;
    private int $expiration;
    private bool $useCache = true;
    private int $page;
    private int $pageSize;
    private string $currentId;

    /**
     * Constructor for PokemonCardService.
     *
     * @param string $apiKey The API key for accessing Pokemon data.
     * @param int $expiration The expiration time for caching data.
     * @param CacheInterface $cache The cache interface for storing data.
     * @param bool $useCache Flag to determine if caching is enabled.
     */
    public function __construct(
        string $apiKey,
        int $expiration,
        CacheInterface $cache,
        bool $useCache = false
    ) {
        $this->useCache = $useCache;
        $this->cache = $cache;
        $this->expiration = $expiration;
        $this->apiKey = $apiKey;
        Pokemon::Options(['api_key' => $this->apiKey]);
    }

    protected function getAllCardsFromApi()
    {
        $page = $this->page;
        $pageSize = $this->pageSize;
        return Pokemon::Card()->where([
            'set.legalities.standard' => 'legal'
        ])->page($page)->pageSize($pageSize)->all();
    }

    protected function getAllCardsWithoutCache()
    {
        return $this->getAllCardsFromApi();
    }

    protected function getAllCardsWithCache()
    {
        $page = $this->page;
        $pageSize = $this->pageSize;
        $cacheKey = sprintf('pokemon_card_list_%d_%d', $page, $pageSize);
        return $this->cache->get($cacheKey, function (ItemInterface $item) use ($page, $pageSize) {
            $item->expiresAfter($this->expiration);
            return $this->getAllCardsFromApi($page, $pageSize);
        });
    }

    protected function getCardByIdWithCache()
    {
        $id = $this->currentId;
        $cacheKey = sprintf('pokemon_card_%s', $id);
        return $this->cache->get($cacheKey, function (ItemInterface $item) use ($id) {
            $item->expiresAfter($this->expiration);
            return $this->getCardByIdFromApi($id);
        });
    }

    protected function getCardByIdWithoutCache()
    {
        return $this->getCardByIdFromApi();
    }

    protected function getCardByIdFromApi()
    {
        $id = $this->currentId;
        return Pokemon::Card()->find($id);
    }

   /**
     * Retrieves all Pokemon cards from the API.
     *
     * @return array The array of Pokemon card data.
     */
    public function getAllCards(int $page, $pageSize): array
    {
        $this->page = $page;
        $this->pageSize = $pageSize;
        if (!$this->useCache) {
            return $this->getAllCardsWithoutCache();
        }
        return $this->getAllCardsWithCache();
    }

    /**
     * Retrieves a specific Pokemon card by ID.
     *
     * @param string $id The ID of the Pokemon card to retrieve.
     * @return Card|null The Pokemon card object if found, null otherwise.
     */
    public function getCardById(string $id): ?Card
    {
        $this->currentId = $id;
        if (!$this->useCache) {
            return $this->getCardByIdWithoutCache();
        }
        return $this->getCardByIdWithCache();
    }
}
