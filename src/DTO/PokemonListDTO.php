<?php

namespace App\DTO;

use \ArrayIterator;
use \IteratorAggregate;
use \Traversable;

use Pokemon\Models\Card;

use App\DTO\PokemonListItemDTO;

class PokemonListDTO implements IteratorAggregate
{
    private array $items = [];

    public function __construct(array $items)
    {
    	foreach ($items as $item) {
            $listItem = new PokemonListItemDTO($item);
            $card = new PokemonListItemDTO($item);
    		$this->add($item);
    	}
    }

    public function toArray(): array
    {
        $array = [];
        foreach ($this->items as $item) {
            $array[] = $item;
        }
        return $array;
    }

    public function add(Card $item): void
    {
        $listItem = new PokemonListItemDTO($item);
        $this->items[] = $listItem;
    }

    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->items);
    }
}

