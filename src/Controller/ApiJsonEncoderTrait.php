<?php

namespace App\Controller;

use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Trait for encoding JSON data using the JsonEncoder.
 *
 * This trait provides a method to encode a Card object into a cleaned JSON string.
 */
trait ApiJsonEncoderTrait
{

    private function replaceUnicodeQuotes($data)
    {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $data[$key] = $this->replaceUnicodeQuotes($value);
            }
        } elseif (is_string($data)) {
            $data = str_replace('\\u0022', '"', $data);
        }
        return $data;
    }

    /**
     * Encodes a Card object into a cleaned JSON string.
     *
     * @param Card $card The Card object to be encoded.
     * @return string The cleaned JSON string representation of the Card object.
     */    
	public function encodeJson(array $card): string
	{
        $jsonEncoder = new JsonEncoder();
        $data = $jsonEncoder->encode($card, JsonEncoder::FORMAT);
        $decodedData = json_decode($data, true);
        $cleanedData = $this->replaceUnicodeQuotes($decodedData);
        $cleanedJson = json_encode($cleanedData, JSON_UNESCAPED_UNICODE);

        return $cleanedJson;
	}
}
