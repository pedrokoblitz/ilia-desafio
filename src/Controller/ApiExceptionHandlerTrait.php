<?php

namespace App\Controller;

use \Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Trait for handling exceptions in API controllers.
 *
 * This trait provides a method to handle exceptions by returning a JSON response with an error message.
 */
trait ApiExceptionHandlerTrait
{
    /**
     * Handles exceptions by returning a JSON response with an error message.
     *
     * @param Exception $e The exception to handle.
     * @return JsonResponse The JSON response containing the error message.
     */	
	public function handleException(Exception $e)
	{
		return new JsonResponse(['error' => "Error decoding JSON: " . $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
	}
}